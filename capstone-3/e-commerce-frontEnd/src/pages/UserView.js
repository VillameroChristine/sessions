import React, { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import ProductSearch from '../components/ProductSearch';


export default function UserView({productData}) {

    const [products, setProducts] = useState([])

    useEffect(() => {
        const productssArr = productData.map(products => {
            //only render the active products
            if(products.isActive === true) {
                return (
                    <ProductCard productProp={products} key={products._id}/>
                    )
            } else {
                return null;
            }
        })

        //set the products state to the result of our map function, to bring our returned products component outside of the scope of our useEffect where our return statement below can see.
        setProducts(productssArr)

    }, [productData])

    return(
        <>
            <ProductSearch />
            { products }
        </>
        )
}