import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView(){

	const {user} = useContext(UserContext);

	const {productId} = useParams();
	const navigate = useNavigate();
	const [name, setproductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const addToCart = (productId) => {
		fetch(`https://capstone2-villamero7.onrender.com/users/to-order`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			if(data){
				Swal.fire({
					title: "Successfully added to Cart!",
					icon: "success",
					text: "You have successfully added this product to your cart."
				})

				navigate("/products/all");
			}else{
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {

		fetch(`https://capstone2-villamero7.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			 setproductName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])


	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 2 }}>
                    <Card>
                        <Card.Body className="text-center">
						<Card.Subtitle>Product name:</Card.Subtitle>
							<Card.Text>{name}</Card.Text>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>

                            {
                            	user.id !== null ?
                            	<Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
                            	:
                            	<Link className="btn btn-danger btn-block" to="/login">Login to Order</Link>
                            }

                            
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}