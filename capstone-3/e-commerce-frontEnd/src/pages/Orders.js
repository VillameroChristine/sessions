import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';

import UserContext from '../UserContext';
import OrderView from './AdminOrderViewView';

export default function Orders() {

    const { userId, setUser } = useContext(UserContext);


    // State that will be used to store the products retrieved from the database
    const [orders, setOrders] = useState([]);


    const fetchData = () => {
        fetch(`https://capstone2-villamero7.onrender.com/users/getOrders`)
        .then(res => res.json())
        .then(data => {

            setUser(userId);

        });
    }


    useEffect(() => {

        fetchData()

    }, []);


    return(
        <>
            
                (user.isAdmin === true) ?
                    <OrderView productData={products} fetchData={fetchData} />

        </>
    )
}