import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';

import UserContext from '../UserContext';
import UserView from './UserView';
import AdminView from './AdminView';

export default function Products() {

    const { user } = useContext(UserContext);

    // State that will be used to store the products retrieved from the database
    const [products, setProducts] = useState([]);


    const fetchData = () => {
        fetch(`https://capstone2-villamero7.onrender.com/products/all`)
        .then(res => res.json())
        .then(data => {

            setProducts(data);
        });
    }

    useEffect(() => {

        fetchData()

    }, []);


    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView productData={products} fetchData={fetchData} />

                    :

                    <UserView productData={products} />

            }
        </>
    )
}