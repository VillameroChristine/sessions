import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function MyOrders(){

    const navigate = useNavigate();
    const {productId} = useParams();
	const [productName, setproductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(0);
	const [status, setStatus] = useState(0);
    const [orders, setOrders] = useState([]);

    

    const fetchData = () => {
        fetch(`https://capstone2-villamero7.onrender.com/users/getCartOrders`,{
            headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
        })
        .then(res => res.json())
        .then(data => {

			console.log(data)


        setOrders(data.map(item => {
            return (
                <Row>
            
                <Col lg={{ span: 6, offset: 1 }} >
                    <Card className="bg-light text-black p-2 m-2">
                        <Card.Body className="text-left">
                        <Card.Subtitle className="text-center">Imagine image here</Card.Subtitle>
                        <Card.Subtitle>Product name:</Card.Subtitle>
                            <Card.Text>{item.productName}</Card.Text>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{item.description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {item.price}</Card.Text>
                            <Card.Subtitle>Quantity: {item.quantity}</Card.Subtitle>     
                            <Card.Subtitle className="mt-2">Quantity: {item.status}</Card.Subtitle>                           

                        </Card.Body>        
                    </Card>
                </Col>

            </Row>
            )
        }) )

        });
    }

	useEffect(() => {

        fetchData()

    }, []);

    return(
        <Container className="mt-5 bg-secondary">
            
          <Row>

            {orders}
                <Button variant="warning" size="md" className='mt-3 p-3'>Continue shopping</Button>

        </Row>  


        </Container>
        )
};








