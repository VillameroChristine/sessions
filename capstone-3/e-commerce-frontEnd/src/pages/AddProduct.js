import {useState,useEffect, useContext} from 'react';
import {Form, Button, Row, Col, Container, Card} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct(){

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    //input states
    const [name,setName] = useState("");
    const [description,setDescription] = useState("");
    const [price,setPrice] = useState("");

    function createProduct(e){

        //prevent submit event's default behavior
        e.preventDefault();

        let token = localStorage.getItem('token');
        console.log(token);

        fetch('https://capstone2-villamero7.onrender.com/products/addProduct',{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                name: name,
                description: description,
                price: price

            })
        })
        .then(res => res.json())
        .then(data => {

            //data is the response of the api/server after it's been process as JS object through our res.json() method.
            console.log(data);

            if(data){
                Swal.fire({

                    icon:"success",
                    title: "Product Added"

                })

                navigate("/addProduct");
            } else {
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful Product Creation",
                    text: data.message

                })
            }

        })

        setName("")
        setDescription("")
        setPrice(0);
    }

    return (

            (user.isAdmin === true)
            ?
            <>
            <Container>
                <Row className="mt-5 justify-content-center">
                    <Col lg={6}>
                        <Card>
                            <Card.Body>
                            <h1 className="my-5 text-center">Add Product</h1>
                <Form onSubmit={e => createProduct(e)}>
                    <Form.Group>

                        <Form.Control type="text" placeholder="Enter Name of product" className="m-2" required value={name} onChange={e => {setName(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>

                        <Form.Control type="text" placeholder="Enter Description" className="m-2" required value={description} onChange={e => {setDescription(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>

                        <Form.Control type="number" placeholder="Enter Price" className="m-2" required value={price} onChange={e => {setPrice(e.target.value)}}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" className="m-4">Submit</Button>
                </Form>

                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
                
            </>
            :
            <Navigate to="/all" />

    )


}