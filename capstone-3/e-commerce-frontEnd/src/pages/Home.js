import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import ProductCard from '../components/ProductCard';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "BTools",
        content: "Beauty Tools and Brushes",
        /* buttons */
        destination: "/products/all",
        label: "Check these products now!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}