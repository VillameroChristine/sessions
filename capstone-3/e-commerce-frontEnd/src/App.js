import AppNavbar from './components/AppNavbar';
import Products from "./pages/Products";
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import ProductView from './pages/ProductView';
import AdminView from './pages/AdminView';
import AddProduct from './pages/AddProduct';
import ProductCard from './components/ProductCard';
import ResetPassword from './components/ResetPassword';
import UpdateProfile from './components/UpdateProfile';
import PreviewCart from './components/PreviewCart';
import OrderViewCustomer from './pages/OrderViewCustomer';
import OrderViewAdmin from './pages/OrderViewAdmin';


//BrowserRouter, Routes, Route works together to setup components endpoints
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
import {useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';
import './App.css';
import Error from './pages/Error';

import {UserProvider} from './UserContext';



function App() {

  //Gets the token from local storage and assigns the token to user state
  const[user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  });

  //unsetUser is a function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
  })

  return (

    //The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components.
    <UserProvider value = {{user, setUser, unsetUser}}>

    <Router>

      <Container fluid>
       <AppNavbar />

       <Routes>
        <Route path="/" element={ <Home /> } />
        <Route path="/products/all" element={ <Products /> } />
        <Route path="/products/:productId" element={ <ProductView /> } />
        <Route path="/login" element={ <Login /> } />
        <Route path="/profile" element={ <Profile /> } />
        <Route path="/users/previewCart" element={ <PreviewCart /> } />
        <Route path="/myOrders" element={ <OrderViewCustomer /> } />
        <Route path="/orderView" element={ <OrderViewAdmin /> } />
        <Route path="/users/reset-password" element={ <ResetPassword /> } />
        <Route path="/users/profile" element={ <UpdateProfile /> } />
        <Route path="/logout" element={ <Logout /> } />
        <Route path="/register" element={ <Register /> } />
        <Route path="/adminView" element={ <AdminView /> } />
        <Route path="/addProduct" element={ <AddProduct /> } />
        <Route path="*" element={<Error />} />
        
       </Routes>

      </Container>

    </Router>
    </UserProvider>
  );
  }

export default App;
