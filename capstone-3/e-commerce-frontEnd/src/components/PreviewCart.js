import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Cart(){

    const navigate = useNavigate();
    const {productId} = useParams();
	const [productName, setproductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [status, setStatus] = useState(0);
    const [quantity, setQuantity] = useState(0);
    const [orders, setOrders] = useState([]);

    const deleteItem = (productId) => {
        fetch(`https://capstone2-villamero7.onrender.com/orders/${productId}/deleteItem`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {

            if(data) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Product successfully disabled'
                })
                fetchData();

            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    };

    const fetchData = () => {
        fetch(`https://capstone2-villamero7.onrender.com/users/getCartOrders`,{
            headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
        })
        .then(res => res.json())
        .then(data => {


            // console.log(data[0].productName);
            // console.log("HAHA");
        //   setName(data.productName);
		// 	setDescription(data.description);
        //   setQuantity(data.quantity);
		// 	setPrice(data.price);

        setOrders(data.map(item => {
            return (
                <Row>
            
                <Col lg={{ span: 6, offset: 1 }} >
                    <Card className="bg-primary text-black p-2 m-2">
                        <Card.Body className="text-left">
                        <Card.Subtitle className="text-center">Imagine image here</Card.Subtitle>
                        <Card.Subtitle>Product name:</Card.Subtitle>
                            <Card.Text>{item.productName}</Card.Text>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{item.description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {item.price}</Card.Text>
                            <Card.Subtitle>Quantity: {item.quantity}</Card.Subtitle>         
                            <Card.Subtitle className="mt-2">Status: {item.status}</Card.Subtitle>               
                            {/* <Button variant="danger" size="sm" className='mt-3' >Delete item</Button> */}
                            <Button variant="danger" size="sm" className='mt-3' onClick={() => deleteItem(productId)} >Delete item</Button>

                        </Card.Body>        
                    </Card>
                </Col>

            </Row>
            )
        }) )

        });
    }

    
    const checkOut = () => {
		fetch(`https://capstone2-villamero7.onrender.com/orders/checkOut`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				description: description,
                Quantity: quantity,
				price: price,
                totalAmount: quantity * price
			})
		})
		.then(res => res.json())
		.then(data => {

            console.log("hatdogggg")
            console.log(data)

			if(data){
				Swal.fire({
					title: "Order succesfully placed!",
					icon: "success",
					text: "You have successfully placed your order. Thank you!"
				})

				navigate("/products/all");
			}else{
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

    useEffect(() => {

        fetchData()

    }, []);

    return(
        <Container className="mt-5 bg-secondary">
            {orders}
          <Row>
            <Col className="d-flex justify-content-end">
                <Button variant="light" size="md" className='my-5 p-3' onClick={() => checkOut(productId)}>Check Out Order</Button>
            </Col>
        </Row>  


        </Container>
        )
}








