import { Row, Col, Card, Container } from 'react-bootstrap';

export default function Hightlights(){
	return (
		<Container>
			<Row className="mt-3 mb-3">
			<Col xs={12} md={4} >
				<Card className="cardHighlight p-5 bg-dark text-white">
			      <Card.Body className=" cardbody primary" >
			        <Card.Title>
			        	<h2>Perfect for Holiday Gifts</h2>
			        </Card.Title>
			        <Card.Text>
			          Snap up the hottest beauty gifts before they're gone!
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4} >
				<Card className="cardHighlight p-5 bg-dark text-white">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Emphasize your Eyes</h2>
			        </Card.Title>
			        <Card.Text>
			         Check out these eye beauty tools that will enhance your eyes appearance!
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4} >
				<Card className="cardHighlight p-5 bg-dark text-white">
			      <Card.Body className=" p-5">
			        <Card.Title>
			        	<h2>Check out these fragrances</h2>
			        </Card.Title>
			        <Card.Text>
			          Spritz on the latest, most coveted scents from indie and iconic brands.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

		</Row>
		</Container>
		
	)
}