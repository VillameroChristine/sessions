import React, { useState } from 'react';
import { Card, Button, Row, Col, Container } from 'react-bootstrap';
import ProductCard from './ProductCard';
const Productsearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://capstone2-villamero7.onrender.com/products/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for product:', error);
    }
  };

  return (

    <Container>
			<Row>
				<Col lg={{ span: 8, offset: 2 }}>
              <h3 className="mt-5">Product Search</h3>
            <div className="form-group">
              <input
                type="text"
                id="productName"
                className="form-control m-2"
                placeholder="Enter product name"
                value={searchQuery}
                onChange={event => setSearchQuery(event.target.value)}
              />
            </div>
            <button className="btn btn-primary my-4" onClick={handleSearch}>
              Search
            </button>
            <h2>Search Results:</h2>
            <ul>
              {searchResults.map(product => (

                <div className="p-3 bg-success">
              <ProductCard productProp={product} key={product._id}/>
              </div>
              ))}
            </ul>
				</Col>
			</Row>
		</Container>

      

  );
};

export default Productsearch;