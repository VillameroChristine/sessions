import { Card, Button, Row, Col, Container } from 'react-bootstrap';
import productData from '../data/productData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { _id, name, description, price } = productProp;

	return (
		<Container>
			<Row>
				<Col lg={{ span: 8, offset: 2 }}>
					<Card className="p-2 m-2 bg-warning text-dark">
					<Card.Body>
						<Card.Title>Name:</Card.Title>
						<Card.Text className="m-2">{name}</Card.Text>
						<Card.Title >Description:</Card.Title>
						<Card.Text className="m-2">{description}</Card.Text>
						<Card.Title>Price:</Card.Title>
						<Card.Text className="m-2">Php {price}</Card.Text>
						<Link className="btn btn-dark" to={`/products/${_id}`}>Details</Link>
					</Card.Body>
					</Card>	
				</Col>
			</Row>
		</Container>
	
	)
}

ProductCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}