import { useState, useContext } from 'react';
import {Container, Navbar, Nav} from 'react-bootstrap';

import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

    const { user } = useContext(UserContext);

    return(
        
        <Navbar expand="lg" className='bg-dark'>
            <Container fluid>
                <Navbar.Brand as={Link} to="/" className=" text-white">bTools</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto text-white">
                        <Nav.Link as={NavLink} to="/" exact className=" text-white">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/products/all" exact className=" text-white">Products</Nav.Link>
                        {(user.id !== null) ? 

                                user.isAdmin 
                                ?
                                <>
                                    <Nav.Link as={Link} to="/addProduct" className=" text-white">Add Product</Nav.Link>
                                    <Nav.Link as={Link} to="/orderView" className=" text-white">View Orders</Nav.Link>
                                    <Nav.Link as={Link} to="/logout" className=" text-white">Logout</Nav.Link>
                                </>
                                :
                                <>
                                    <Nav.Link as={Link} to="/profile" className=" text-white">Profile</Nav.Link>
                                    <Nav.Link as={Link} to="/logout" className=" text-white">Logout</Nav.Link>
                                </>
                            : 
                                <>
                                    <Nav.Link as={Link} to="/login" className=" text-white">Login</Nav.Link>
                                    <Nav.Link as={Link} to="/register" className=" text-white">Register</Nav.Link>
                                </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        )
}