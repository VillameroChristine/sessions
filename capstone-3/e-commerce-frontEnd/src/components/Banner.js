import { Button, Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// data is received through parameter
export default function Banner({data}) {

	// Object destructuring
    const {title, content, destination, label} = data;

    return (
        <Container>
            <Row className="m-4 p-3">
            <Col className="p-5 text-center bg-warning">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link className="btn btn-dark" to={destination}>{label}</Link>
            </Col>
        </Row>

        </Container>
        
    )
};
