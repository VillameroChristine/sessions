let productsDatabase = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec tortor ac sapien dictum semper at at augue. Vestibulum eget lorem non lorem vehicula tincidunt. Quisque suscipit hendrerit auctor. Maecenas at lacus nibh. Vestibulum id accumsan ex, id sagittis ante. Donec risus purus, semper consequat enim sed, gravida pulvinar odio.",
		price: 45000,
		onOffer: true
	}, 
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Quisque dignissim consectetur eros, nec efficitur odio bibendum sit amet. Mauris ac euismod risus. Duis tempor eros quis urna malesuada imperdiet. Nunc fringilla bibendum congue.",
		price: 50000,
		onOffer: true
	}, 
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Curabitur vel elementum urna, molestie ornare lorem. Etiam maximus elit quis posuere fringilla. Duis sollicitudin mi sapien, in rutrum orci pharetra id. Phasellus vulputate, odio id sagittis sollicitudin, neque ipsum euismod purus, eu iaculis ex lacus id massa.",
		price: 55000,
		onOffer: true
	}
];

export default productsDatabase;