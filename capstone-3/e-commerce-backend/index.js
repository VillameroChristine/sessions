// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

const userRoutes = require('./routes/user_route'); 
const productRoutes = require("./routes/product_route.js")
const orderRoutes = require("./routes/order_routes.js")


const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.use(cors()); // CORS is a security feature implemented by web browsers to control requests to a different domain than the one the website came from.


mongoose.connect('mongodb+srv://villamerosam:villamerosam123@cluster0.bpwwjas.mongodb.net/E-commerce-backend?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));


app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${ process.env.PORT || port }`);
	});
};

module.exports = app;