const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order_controller.js');
const auth = require('../auth.js');

//Destructure from auth

const {verify, verifyAdmin} = auth;

// Adding product by Admin Only
router.get("/myOrders", verify, orderController.viewOrder);

router.get("/view-orders", verify, verifyAdmin, orderController.viewOrderAdmin);

router.post("/checkOut", verify, orderController.checkOut, orderController.afterCheckOut );

/*Clear All Items From Cart*/
router.put('/clearCart', verify, orderController.ClearCart);

/*Remove From Order*/
router.put('/archiveOrder', verify, verifyAdmin, orderController.RemoveOrder);

/*Remove Item*/
router.put('/:productId/deleteItem', verify, orderController.deleteItem);

module.exports = router;