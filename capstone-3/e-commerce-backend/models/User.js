//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    orders : [
        {
            productName : {
                type : String,
                required : [true, "product Name is required"]
            },
            productId : {
                type : String,
                required : [true, "product ID is required"]
            },
            description : {
                type : String,
                required : [true, "Description is required"]
            },
            quantity : {
                type : Number,
                default : 1
            },
            price : {
                type : Number,
                required : [true, "Price is required"]
            },
            status : {
                type : String,
                default : "in stock!"
            }
        }
    ]
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);