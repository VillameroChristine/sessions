const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

            userId:{
                type: String,
                required: [true, 'UserId is required!']
            },
            productName:{
                type: String,
                required: [true, 'ProductName is required!']
            },
            productId:{
                type: String,
                required: [true, 'ProductId is required!']
            },
            quantity:{
                type: Number,
                default: 1
            },
            price:{
                type: Number,
                required: [true, 'Price is required!']
            },
            totalAmount:{
                type: Number,
                required: [true, 'Total amount is required!']
            },
            dateCheckedOut: {
                type: Date,
                default: new Date()
            },
            order_Status: {
                type: String,
                default: "Order placed" 
            }  
});

module.exports = mongoose.model("Orders", orderSchema);


