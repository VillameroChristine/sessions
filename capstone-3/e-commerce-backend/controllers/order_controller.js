const User = require('../models/User.js');
const Orders = require('../models/Orders.js');
const Product = require('../models/Product.js');
const { response } = require('../index.js');

//Adding order by Customers 

module.exports.viewOrder =  (req, res)=> {

		return Orders.find({}).then(result =>{
            if(result.length === 0){
                return res.send("There are currently no orders in the db.")
            }else{
                return res.send(result);
                }
            })
            .catch(err => err);

};

module.exports.viewOrderAdmin =  (req, res)=> {

	return Orders.find({}).then(result =>{
		if(result.length === 0){
			return res.send("There are currently no orders in the db.")
		}else{
			return res.send(result);
			}
		})
		.catch(err => err);

};


//============================================================================

module.exports.deleteItem = (req, res) => {
	let updateActiveField = {
		productId: false
	}

	return Orders.findByIdAndUpdate(req.params.productId, updateActiveField).then((product, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}

/*Remove from Order*/
module.exports.RemoveOrder = (request, response) => {
	let productId_ = request.body.orderId;
	let UserID = request.user.id;

	return Order.findByIdAndUpdate(productId_, {
		isActive: false
	}).then((result, saveErr) => {
		if(saveErr){
			return response.send(`Order not Removed! Please try again.`);
		}else{
			return response.send(`${productId_} successfully removed from your list of orders.`);
		}
	});
};


/*Check-Out User*/
module.exports.checkOut = async (request, response, next) => {
	if(request.user.isAdmin){
		return response.send('Only non-admin accounts are allowed.');
	}
	let GetAllItemsOnCart =  await User.findById(request.user.id).then((result, error) => {
		let returnArray = [];
		result.orders.forEach((item) => {
			returnArray.push({
				productName: item.productName,
				productId: item.productId,
				quantity: item.quantity,
				price: item.price,
				totalAmount: item.price * item.quantity

			});

		});
		return response.send(returnArray);
		});

	// if(GetAllItemsOnCart.length < 1){
	// 	return response.send("No Item on Cart");
	// }
	
	console.log("GetAllItemsOnCart")
	console.log(GetAllItemsOnCart[0]['productId'])
		
		for(x=0; x <= GetAllItemsOnCart.length;x++){

			let newOrder = new Orders ({
				userId: request.user.id,
				productId: GetAllItemsOnCart[x]['productId'],
				productName: GetAllItemsOnCart[x]['productName'],
				quantity: GetAllItemsOnCart[x]['quantity'],
				price: GetAllItemsOnCart[x]['price'],
				totalAmount: GetAllItemsOnCart[x]['quantity'] *  GetAllItemsOnCart[x]['price']
			});
			
			 newOrder.save().then((savedTask, saveErr) => {

				if(saveErr){
					return response.send("hoho");
				}else{
					return response.send("hehe");
					 next();
				}
			}).catch(err => response.send(err));
		}	

	};



/*Deleting all items on cart after Check-Out*/
module.exports.afterCheckOut = (request, response) => {
	return User.findOneAndUpdate({
		_id: request.user.id
	}, 
	{
		$pull: {
			myCart: {
				productId: {
					$ne: ""
				}
			}
		}
	}).then((result, error) => {
		
		//result.enrollments.pop();
		return result.save().then((result, error) => {
			if(error){
				return response.send(error);
			}else{
				return response.send(`Order saved successfully!`);
			}
		}).catch(err => response.send(err));
	});
};


/*Remove from Order*/
module.exports.RemoveOrder = (request, response) => {
	let productId_ = request.body.orderId;
	let UserID = request.user.id;

	return Order.findByIdAndUpdate(productId_, {
		isActive: false
	}).then((result, saveErr) => {
		if(saveErr){
			return response.send(`Order not Removed! Please try again.`);
		}else{
			return response.send(`${productId_} successfully removed from your list of orders.`);
		}
	});
};


/*Deleting all items on cart*/
module.exports.ClearCart = (request, response) => {
	return User.findOneAndUpdate({
		_id: request.user.id
	}, 
	{
		$pull: {
			myCart: {
				productId: {
					$ne: ""
				}
			}
		}
	}).then((result, error) => {
		
		//result.enrollments.pop();
		return result.save().then((result, error) => {
			if(error){
				return response.send(error);
			}else{
				return response.send(`Your cart is now empty.`);
			}
		}).catch(err => response.send(err));
	});
};