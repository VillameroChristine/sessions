// [ SECTION ] Dependecies and Modules
const User = require('../models/User.js');
const Product = require("../models/Product.js");
const bcrypt = require('bcrypt');

const auth = require('../auth');

// module.exports.checkEmailExist = (reqBody) => {
// 	console.log(reqBody);
// 	console.log(reqBody.email);
// 	return User.find({ email: reqBody.email }).then(result => {
// 		console.log(result);
// 		if(result.length > 0){
// 			return true
// 		} else {
// 			return false
// 		};
// 	});
// };

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
	.catch(err => err);
};

// user authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result === null){
			return false;
		} else {

	const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false; 
			}
		}
	})
	.catch(err => res.send(err))
}


module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(error => error)
}

module.exports.addToCart = async (req, res) => {
	if(req.user.isAdmin){
		return res.send("You are not allowed to perform this action, because you are an ADMIN");
	}

	console.log("WOOH");
	console.log(req.body.productName);
	
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newOrder = {
			productId: req.body.productId,
			productName: req.body.productName,
			description: req.body.description,
			price: req.body.price

		}
	
		let listOfOrders = user.orders;
		let filteredproductId = [];

		listOfOrders.forEach((result) => {
		filteredproductId.push(result.productId);
		});

		user.orders.push(newOrder);
		return user.save().then(user => res.send(true))
		.catch(error => {
			res.send(false);
		});

	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isProductUpdated = await Products.findById(req.body.productId).then(product => {
		let buyer = {
			userId: req.user.id
		}

		products.buyers.push(buyer);
		return products.save().then(product => true).catch(error => res.send(error));
	})

	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated});
	}

	if(isUserUpdated && isProductUpdated){
		return res.send({message: true});
	}
}

module.exports.getCartOrders = (req, res) => {
	User.findById(req.user.id).then(result => {
		if(result.orders == [] || result.orders == null){
			return res.send("You dont have any orders.")
		}else{
			return res.send(result.orders);
		}
	}).catch(error => res.send(error));
}


module.exports.resetPassword = async (req, res) => {
	try{
		const newPassword = req.body.newPassword;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPassword, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: "Password reset successfully!"});
	}catch(error){
		res.status(500).json({message: "Internal Server Error"});
	}
}

