import AppNavbar from './components/AppNavbar';
import Courses from "./pages/Courses";
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import CourseView from './pages/CourseView';
import AdminView from './pages/AdminView';

//BrowserRouter, Routes, Route works together to setup components endpoints
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
import {useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';
import './App.css';
import Error from './pages/Error';

import {UserProvider} from './UserContext';



function App() {

  //Gets the token from local storage and assigns the token to user state
  const[user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  });

  //unsetUser is a function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
  })

  return (

    //The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components.
    <UserProvider value = {{user, setUser, unsetUser}}>

    <Router>

      <Container fluid>
       <AppNavbar />

       <Routes>
        <Route path="/" element={ <Home /> } />
        <Route path="/courses" element={ <Courses /> } />
        <Route path="/login" element={ <Login /> } />
        <Route path="/profile" element={ <Profile /> } />
        <Route path="/logout" element={ <Logout /> } />
        <Route path="/register" element={ <Register /> } />
        <Route path="/courses/:courseId" element={ <CourseView /> } />
        <Route path="/adminView" element={ <AdminView /> } />
        <Route path="*" element={<Error />} />
        
       </Routes>

      </Container>

    </Router>
    </UserProvider>
  );
  }

export default App;
