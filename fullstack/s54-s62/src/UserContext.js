import React from 'react';

//creates a context object for sharing data between components
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;