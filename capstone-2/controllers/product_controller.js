const Products = require('../models/Products.js');

// Adding product by Admin Only
module.exports.addProduct = (req, res) => {
    let newProduct = new Products ({
        productName: req.body.productName,
        brandName: req.body.brandName,
        description: req.body.description,
        price: req.body.price
    });

    return newProduct.save().then((product, error) => {
        if (error){
            res.send("Product is NOT added!")
        }else{
            res.send("Product added!")
        }
    })
    .catch(err => err);
};


module.exports.viewAllProducts = (req, res) => {
    return Products.find({}).then(result =>{
        if(result.length === 0){
            return res.send("There are no products in the db.")
        }else{
            return res.send(result);
        }
    })
};


module.exports.getAllActive = (req, res) => {
    return Products.find({HasStock : true}).then(result => {
        if(result.length === 0){
            return res.send("There are currently no product stocks");
        }else{
            return res.send(result);
        }
    })
};


module.exports.getProduct = (req, res) => {
    return Products.findById(req.params.productId).then(result =>{
        console.log(result)
        if(!result){
            return res.send("Cannot find product with the ID provided.")
        }else{
            if(result.HasStock === false){
                return res.send ("the product you are trying to search is currently not available")
            }else{
                return res.send(result)
            }
        }
    })

    .catch(error => res.send("Please enter a correct product ID"));
};


module.exports.updateProduct = (req, res) =>{
    let updatedProduct = {
        productName: req.body.productName,
        brandName: req.body.brandName,
        description: req.body.description,
        price: req.body.price
    }
    
    return Products.findByIdAndUpdate(req.params.productId, updatedProduct)
    .then((product, error) =>{
        if(error){
            return res.send(false);
        }else{
            return res.send("Product is successfully updated!");
        }
    })
};


module.exports.archiveProduct = (req,res) => {
    let archivedProduct = {
        HasStock: req.body.HasStock
    }
    return Products.findByIdAndUpdate(req.params.productId, archivedProduct)
    .then((product,error) => {
        if(error){
            return res.send(false)
        }else{
            return res.send("Product is archived.")
        }
    })
};


module.exports.activateProduct = (req,res) => {
    let activatedProduct = {
        HasStock: true
    }
    return Products.findByIdAndUpdate(req.params.productId, activatedProduct)
    .then((product,error) => {
        if(error){
            return res.send(false)
        }else{
            return res.send("Product is activated!")
        }
    })
};


// module.exports.archivedProduct = (req, res) => {
//     return Products.find({HasStock : false}).then(result => {
//         if(result.length === 0){
//             return res.send("There are currently no archived products");
//         }else{
//             return res.send(result);
//         }
//     })
// };

module.exports.DeletedProduct = (req, res) => {
    Products.find({ HasStock: false })
        .then(result => {
            if (result.length === 0) {
                return res.status(404).send("There are currently no archived products");
            } else {
                return res.status(200).send(result);
            }
        })
        .catch(error => {
            console.error(error);
            return res.status(500).send("An error occurred while fetching archived products");
        });
};