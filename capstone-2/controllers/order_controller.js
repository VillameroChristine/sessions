const User = require('../models/Users.js');
const Orders = require('../models/Orders.js');
const Product = require('../models/Products.js');
const { response } = require('../index.js');

//Adding order by Customers 

module.exports.viewOrder =  (req, res)=> {
    if(req.user.isAdmin){
		return Orders.find({}).then(result =>{
            if(result.length === 0){
                return res.send("There are currently no orders in the db.")
            }else{
                return res.send(result);
                }
            })
            .catch(err => err);
    }else{
        return res.send("you cant do this action.")
        .catch(err => err);
        }
};


//Adding order by Customers 
module.exports.addToCart = (req, res) => {
	if (req.user.isAdmin) {
        return res.status(403).send('Only customers can perform this action.');
    }
    return User.findById(req.user.id).then(async (result, error) => {

       let getPrice = await Product.findById(req.body.productId).then((result)=>{
            return result.price;
       })

        let newOrder = {
            productId:req.body.productId,
            productPrice: getPrice,
            quantity:req.body.quantity,
			totalAmount:getPrice * req.body.quantity
        }
        result.orders.push(newOrder);
        return result.save().then((order, error) => {
         if (error){
           return res.send("Order not successful!")
            
        }else{
           return res.send("Added to cart!")
        }
   })
  
    })
};

//============== CHECKING OUT AND EMPTYING CART =======================
// module.exports.checkOut = async (request, response) => {
//     if (request.user.isAdmin) {
//         return response.status(403).send('Only customers can perform this action.');
//     }
//         const user = await User.findById(request.user.id);

//         if (!user) {
//             return response.status(404).send("User not found.");
//         }

//         if (user.orders.length < 1) {
//             return response.send("No items in the cart.");
//         }

// 		console.log(user.orders.length) 

//         let itemsToOrder = [];
//         user.orders.forEach((item) => {
//           let newOrder = new Orders({
// 			userId: request.user.id,
//             productId: item.productId,
//             quantity: item.quantity,
//             price: item.productPrice
// 			//totalAmount:item.totalAmount
//           });
          
//           newOrder.save().then(result =>  response.send(result))
//         });
        
//         // Clear the cart after checkout
//        // user.orders = [];
//        // await user.orders.save();

//         return response.status(200).send('Order saved successfully.');
  
// };

module.exports.checkOut = async (request, response) => {
    if (request.user.isAdmin) {
        return response.status(403).send('Only customers can perform this action.');
    }


        const user = await User.findById(request.user.id);

        if (!user) {
            return response.status(404).send("User not found.");
        }

        if (user.orders.length < 1) {
            return response.send("No items in the cart.");
        }

		console.log(user.orders.length) 

        let itemsToOrder = [];
        user.orders.forEach((item) => {
          let newOrder = new Orders({
			userId: request.user.id,
            productId: item.productId,
            quantity: item.quantity,
            price: item.productPrice
          });
          
          newOrder.save().then(result => response.send(result))
        });
        
        // Clear the cart after checkout
        user.orders = [];
       // await user.orders.save();

        return response.status(200).send('Order saved successfully.');
    // } catch (error) {
    //     return response.status(500).send(`Error: ${error.message}`);
    // }
};
