const Carts = require('../models/Carts.js');
const User = require('../models/Users.js');
const Order = require('../models/Orders.js');
const { resource } = require('../index.js');

// Adding order by Customers 
module.exports.addToCart = (req, res) => {
     let newOrder = new Carts ({
        userId: req.body.userId,
        productId: req.body.productId,
        price:req.body.price,
        quantity: req.body.quantity
    });
    return newOrder.save().then((order, error) => {
        if (error){
            return res.send("Order not successful!")
             
        }else{
            return res.send("Added to cart!")
            
        }
    })
   
    .catch(err => err);
};


module.exports.checkOut = async (req, res, next) => {
	if(req.user.isAdmin){
		return res.send('Only non-admin accounts are allowed.');
	}
	let GetAllItemsOnCart =  await User.findById(req.user.id).then((result, error) => {
		let returnArray = [];
		result.myCart.forEach((item) => {
			returnArray.push({
				productId: item.productId,
				quantity: item.quantity,
				price: item.price
			});
		});
		return returnArray;
		});

	if(GetAllItemsOnCart.length < 1){
		return res.send("No Item on Cart");
	}

	let newOrder = new Order({
			userId: req.user.id,
			productsPurchased: GetAllItemsOnCart
		});

		return newOrder.save().then((savedTask, saveErr) => {
			if(saveErr){
				return res.send(`Order not saved! Please try again.`);
			}else{
				next();
			}
		}).catch(err => res.send(err));
};

/*Deleting all items on cart after Check-Out*/
module.exports.afterCheckOut = (req, res) => {
	return User.findOneAndUpdate({
		_id: req.user.id
	}, 
	{
		$pull: {
			myCart: {
				productId: {
					$ne: ""
				}
			}
		}
	}).then((result, error) => {
		
		//result.enrollments.pop();
		return result.save().then((result, error) => {
			if(error){
				return res.send(error);
			}else{
				return res.send(`Order saved successfully!`);
			}
		}).catch(err => res.send(err));
	});
};










// Checkout
// module.exports.checkOut = async (req, res) => {
//     try {
//         let newOrder = new Order({
//             userId: req.body.userId,
//             productId: req.body.productId,
//             quantity: req.body.quantity,
//             price: req.body.price,
//             totalAmount:req.body.price * req.body.quantity
//         });

//         return newOrder.save().then((savedTask, saveErr) => {
//         	if(!saveErr){
//              	return res.send('Your order has been placed. Thank you!');
//         	}else{
//             	return res.send('Order not successful.');
//         	}
//         }).catch(err => err);



//     } catch (error) {
//         console.error(error);
//         return res.status(500).send('Internal server error.');
//     }
// };




