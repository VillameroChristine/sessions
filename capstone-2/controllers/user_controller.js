const User = require('../models/Users.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

//============= user registration =========
 module.exports.registerUser = async (req, res) => {
    try {
        const existingUser = await User.findOne({ email: req.body.email });

        if (existingUser) {
            return res.status(400).send(`${req.body.email} is already used. Try other email.`);
        }

        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            mobileNo: req.body.mobileNo,
            password: bcrypt.hashSync(req.body.password, 10)
        });

        await newUser.save();
        res.status(200).send('User is registered.');
    } catch (error) {
        res.status(500).send('User data is not saved: ' + error.message);
    }
};

//user authentication

module.exports.loginUser = (req, res) => {
    return User.findOne({email: req.body.email}).then(result => {
        if(result == null){
           // return false 
            return res.send("Please provide correct email.") 
        }else{

            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            if(isPasswordCorrect) {
                if(result.isAdmin){
                    return res.send({ 
                        message: "Alright, you are logged in as ADMIN!",
                        YourToken: auth.createAccessToken(result)})
                }else{
                    return res.send({ 
                        message: "Alright, you are logged in as User!",
                        YourToken: auth.createAccessToken(result)})
                }
               
            }else{
                return res.send("Incorrect email or password."); 
            }
        }
    })
    .catch(err => res.send(err))
};


//=======================Getting ALL customer details by admin

module.exports.getCustomerDetails =  (req, res)=> {
    if(req.user.isAdmin){

        return User.find({isAdmin:false}).then(result =>{

          if(result.length === 0){
            return res.send("There are currently no customers in the db.")
         }else{
            return res.send(result);
        }
            }
        )}
    
    return res.send("You cant do this action.")
    .catch(err => res.send(err))
 };

 //=======================Getting ALL user(including admins) details by admin
 module.exports.getAllUsers =  (req, res)=> {
    if(req.user.isAdmin){

        return User.find({}).then(result =>{

          if(result.length === 0){
            return res.send("There are currently no users in the db.")
         }else{
            return res.send(result);
        }
            }
        )}
    
    return res.send("You cant do this action.")
    .catch(err => res.send(err))
 };

 //=======================Getting a customer's detail by admin

module.exports.getCustomerProfile =  (req, res)=> {
    return User.findById(req.params.customerId).then(result =>{
        console.log(result)
        if(!result){
            return res.send("Cannot find customer with the ID provided.")
        }else{
                return res.send(result)
            }
        })
    .catch(error => res.send("Please enter a correct product ID"));
};

//=====================================================================


// //=======Reset password-----
module.exports.resetPassword = async (req, res) => {
    try {
        const newPw = req.body.newPw;
        const userId = req.user.id;

         //Hashing the new password
        const hashedPw = await bcrypt.hash(newPw, 10)

        await User.findByIdAndUpdate(userId, {password: hashedPw});
        res.status (200).json({message: "password reset successfully"});
        }catch(error){
            res.status(500).json({message: "Internal Server Error"});
        }
}