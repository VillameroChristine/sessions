// [ SECTION ] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

const userRoutes = require('./routes/User_routes.js'); 
const productRoutes = require('./routes/Product_routes.js'); 
const orderRoutes = require('./routes/Order_routes.js'); 

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Allows all resources to acces our backend application;
app.use(cors()); 

// [ SECTION ] Database Connection
mongoose.connect('mongodb+srv://villamerosam:villamerosam123@cluster0.bpwwjas.mongodb.net/e-commerce?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [ SECTION ] Backend Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);



// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on port ${port }`);
	});
};


module.exports = app;
