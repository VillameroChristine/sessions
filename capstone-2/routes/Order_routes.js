const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order_controller.js');
const auth = require('../auth.js');

//Destructure from auth

const {verify, verifyAdmin} = auth;

// Adding product by Admin Only
router.get("/view-orders", verify, verifyAdmin, orderController.viewOrder);

router.post("/", verify, orderController.addToCart);

router.post("/checkOut", verify, orderController.checkOut);

module.exports = router;