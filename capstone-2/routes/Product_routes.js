const express = require('express');
const router = express.Router();
const productController = require('../controllers/product_controller.js');
const auth = require('../auth.js')

//Destructure from auth

const {verify, verifyAdmin} = auth;

// Adding product by Admin Only
router.post("/addProduct", verify, verifyAdmin, productController.addProduct);


//View all products
router.get("/view-all", productController.viewAllProducts);


//Get all "active" products
router.get("/activeProducts", productController.getAllActive);


//Get specific product
router.get("/:productId", productController.getProduct);

//Updating product information (admin only)
router.put('/:productId', verify, verifyAdmin, productController.updateProduct);


//Archiving a product
router.put('/:productId/archive', verify, verifyAdmin, productController.archiveProduct);


//Activating a product
router.put('/:productId/activate', verify, verifyAdmin, productController.activateProduct);

//View archived products
router.get('/archivedProducts', verify, verifyAdmin, productController.DeletedProduct);


module.exports = router;