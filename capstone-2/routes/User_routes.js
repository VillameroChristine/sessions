const express = require('express');
const router = express.Router();
const userController = require('../controllers/user_controller.js');
const auth = require('../auth.js')

//Destructure from auth

const {verify, verifyAdmin} = auth;



//user registration routes
router.post("/register", userController.registerUser)


//user authentication
 router.post('/login', userController.loginUser)

 //Get all customer details by Admin only
 router.get("/customerdetails", verify, verifyAdmin, userController.getCustomerDetails);

 //Get all users details by Admin only
 router.get("/usersdetails", verify, verifyAdmin, userController.getAllUsers);

 //Get a customer's detail by Admin only
router.get("/:customerId/details", verify, verifyAdmin, userController.getCustomerProfile);

// //Reset password
router.put('/reset-password', verify, userController.resetPassword)

//get user's course enrollments
// router.get("/getEnrollments", verify, userController.getEnrollments)

module.exports = router;













module.exports = router;