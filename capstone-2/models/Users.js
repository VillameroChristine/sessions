const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'Firstname is required!']
    },
    lastName: {
        type: String,
        required: [true, 'Lastname is required!']
    },
    email: {
        type: String,
        required: [true, 'Email is required!']
    },
    password: {
        type: String,
        required: [true, 'Password is required!']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, 'Mobile number is required!']
    },
    orders: [
        {
            productId:{
                type: String,
                required: [true, 'ProductId is required!']
            },
            shoppedOn: {
                type: Date,
                default: new Date()
            },
            quantity: {
                type: Number,
                required: [true, 'Product price is required']
            },
            productPrice: {
                type: Number,
                required: [true, 'Product price is required']
            },
            // totalAmount: {
            //     type: Number,
            //     required: [true, 'Total amount price is required']
                
            // }
            
        }          
             ]

});

module.exports = mongoose.model("Users", userSchema);