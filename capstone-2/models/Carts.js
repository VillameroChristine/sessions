const mongoose = require('mongoose');

const cartsSchema = new mongoose.Schema({

            userId:{
                type: String,
                required: [true, 'ProductId is required!']
            }, 
            productId:{
                type: String,
                required: [true, 'ProductId is required!']
            },         
            price:{
                type: Number,
                required: [true, 'Product price is required!']
            },
            quantity:{
                type: Number,
                required: [true, 'Product quantity is required!']
            },
            ShoppedOn: {
                type: Date,
                default: new Date()
            }
});

module.exports = mongoose.model("Carts", cartsSchema);