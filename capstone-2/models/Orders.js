const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

            userId:{
                type: String,
                required: [true, 'ProductId is required!']
            },
            productId:{
                type: String,
                required: [true, 'ProductId is required!']
            },
            quantity:{
                type: Number,
                required: [true, 'Product quantity is required!']
            },
            price:{
                type: Number,
                required: [true, 'Price is required!']
            },
            totalAmount:{
                type: Number,
                required: [true, 'Price is required!']
            },
            dateCheckedOut: {
                type: Date,
                default: new Date()
            },
            order_Status: {
                type: String,
                default: "Order placed" 
            }  
});

module.exports = mongoose.model("Orders", orderSchema);


