// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const productSchema = new mongoose.Schema({
	productName : {
		type: String,
		required: [ true, 'Product is required!' ]
	},
	productId : {
		type: String,
		required: [ true, 'Product is required!' ]
	},
    brandName : {
		type: String,
		required: [ true, 'Brandname is required!' ]
	},
	description : {
		type: String,
		required: [ true, 'Description is required!' ]
	},
	price : {
		type: Number,
		required: [ true, 'Price is required!' ]
	},
	quantity : {
		type: Number,
		required: [ true, 'Price is required!' ]
	},
	HasStock : {
		type: Boolean,
		default: true
	},
	AddedOn : {
		type: Date,
		default: new Date()
	},
    
});

// [ SECTION ] Model
module.exports = mongoose.model('Products', productSchema);