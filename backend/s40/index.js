const express = require("express");

const app = express();
const port = 4000;

//Middlewares 
app.use(express.json());
//allows your app to read data from forms

app.use(express.urlencoded({extended:true}));
//accepts all data types


if (require.main === module){
    app.listen(port, ()=> console.log(`Server is running at port ${port}`));
}

//===============end of defaults===================

//[SECTION] Routes

app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.get("/hello", (req, res) => {
    res.send("Hello from /hello endpoint.");
})

app.post("/hello", (req, res) => {
    res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}! `);
})

//POST route to register user
let users =[];

// app.post("/signup", (request, response) => {	
// 	console.log(request.body);
    
// 	if(request.body.username !== "" && request.body.password !== ""){
// 		let doesExist = false;
// 		for(let index = 0 ; index < users.length ; index++){
// 			if(request.body.username === users[index].username){
// 				doesExist = true;
//         break;
// 			}
// 		}
// 		if(doesExist){
// 			response.send("User already exist");
// 		}else{
// 			users.push(request.body)	
// 			response.send(`Hi ${request.body.username}`);
// 		}


// 	}else{
// 		response.send("Username or Password is missing.");
// 	}
// });

app.post("/signup", (req, res) => {
    console.log(req.body);

    
    if (req.body.username !== "" && req.body.password !== ""){
        let exist = false;
        for(let i=0; i< users.length; i++){
            if(req.body.username === users[i].username){
                exist = true;
                break;
            }
        }
        if (exist){
            res.send(`This user is already existed`);
        }else{
            users.push(req.body);
            res.send(`User ${req.body.username} successfully registered`)
        }
        
    }else{
        res.send("Please input BOTH username and password");
    }
})

//Change password - PUT method
app.put("/change-password", (req,res) => {
    let message;
if(users.length > 0){
    for(let i=0; i< users.length; i++){
        if(req.body.username === users[i].username){
            users[i].password = req.body.password;
            message = `User ${req.body.username}'s password has been updated.`;
            break;
        }else if(req.body.username === "" ){
            res.send(`Please enter username! `)
        }else{
            message = "User does not exist."
        }
    }
}else{
    message = "No user registered!"
}

    res.send(message);
})