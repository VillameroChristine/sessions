// CRUD Operations (MongoDB)

// CRUD Operations are the heart of any backend app.
// Mastering CRUD Operations is essential for any developers

/* 
C - Create/insert
R - Retrieve/read
U - update
D - delete
*/

/* ================Inserting/Creating==============

Syntax:
db.users.insertOne({object})


*/

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "09123456789",
        email: "janedoe@gmail.com"
    },
    courses: ["css", "JS", "Python"],
    department: "none"

});

db.users.insertOne({
    firstName: "Christine Mae",
    lastName: "Villamero",
    age: 21,
    contact: {
        phone: "08095419722",
        email: "villamerosam@gmail.com"
    },
    courses: ["css", "JS", "Python"],
    department: "none"

});

db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"

});

/* Inserting multiple objects
//SYNTAX: db.users.insertMany([{object}])

*/

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "09123456789",
            email: "stephenhawking@gmail.com"
        },
        courses: ["PHP", "React", "Python"],
        department: "none"
    },

    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "09123456789",
            email: "neilarmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }

    ]);


/* ===========Read/Retrieve docments===========

Syntax: 
database.collection.findOne();
database.collection.findOne({field: value});
db.users.findOne({object})
db.users.find({object})

*/
db.users.findOne();

db.users.find({department: "none"});


//multiple criteria for specificity

db.users.find({department: "none", age: 82});


//================= Updating =============

/* 
SYNTAX:

db.collectionName.updateOne({criteria}, {$set: {field: value}})

*/

db.users.updateOne(
    {firstName: "Test"}, 
    {$set: {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "09123456789",
            email: "billgates@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }}
);

//finding the data

db.users.find({firstName: "Bill"});

db.users.findOne({firstName: "Test"});
//this results to null


db.users.find({"contact.email": "billgates@gmail.com"});
//this is used when manipulating object inside an object (json like)

//======Update many collection=========

/* 
db.users.updateMany({criteria}, {$set: {field: value}})
*/

db.users.updateMany(
    {department: "none"},
    {
        $set: {
            department: "HR"
        }
    }
);


//=========Delete==============

db.users.insert({
    firstName: "Test"
});

//Delete single data
/* 
SYNTAX:

db.users.deleteOne({criteria})
*/

db.users.deleteOne({
    firstName: "Test"
});

//Delete Many
/* 
SYNTAX:

db.users.deleteMany({criteria})
*/

db.users.deleteMany({
    firstName: "Bill"
});