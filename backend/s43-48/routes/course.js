const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course.js');
const auth = require('../auth.js')

//Destructure from auth

const {verify, verifyAdmin} = auth;

//course registration routes
// router.post("/", (req,res) => {
//     courseController.registerCourse(req.body).then(resultfromController => 
//         res.send(resultfromController))
// });

//Create a course post
router.post("/", verify, verifyAdmin, courseController.addCourse);

//Get all courses
router.get("/all", courseController.getAllCourses);


//Get all "active" courses
router.get("/", courseController.getAllActive)

//Get specific course

router.get("/:courseID", courseController.getCourse);

//Updating a course (admin only)
router.put('/:courseID', verify, verifyAdmin, courseController.updateCourse)


//Archiving a course
router.put('/:courseID/archive', verify, verifyAdmin, courseController.archiveCourse)

router.put('/:courseID/activate', verify, verifyAdmin, courseController.activateCourse)



module.exports = router;