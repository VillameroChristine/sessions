const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.js');
const auth = require('../auth.js')

//Destructure from auth

const {verify, verifyAdmin} = auth;


router.post('/checkEmail', (req, res) => {
    userController.checkEmailExist(req.body).then(resultfromController => res.send(resultfromController));
});


//user registration routes
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(resultfromController => 
        res.send(resultfromController))
});


//user authentication
 router.post('/login', userController.loginUser)

//getting ID
router.post("/details", verify, (req, res) => {
	userController.getProfile(req.body.id).then(resultFromController => res.send(resultFromController));
})

router.get("/details", verify, userController.getProfile);

//Enroll user to a course
router.post("/enroll", verify, userController.enroll);


//get user's course enrollments
router.get("/getEnrollments", verify, userController.getEnrollments)

//Reset password
router.put('/reset-password', verify, userController.resetPassword)














module.exports = router;