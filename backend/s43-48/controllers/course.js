const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


module.exports.addCourse = (req, res) => {
    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    });

    return newCourse.save().then((course, error) => {
        if (error){
            res.send(false)
        }else{
            res.send(true)
        }
    })
    .catch(err => err);
};

module.exports.getAllCourses = (req, res) => {
    return Course.find({}).then(result =>{
        if(result.length === 0){
            return res.send("There is no courses in the db.")
        }else{
            return res.send(result);
        }
    })
};


module.exports.getAllActive = (req, res) => {
    return Course.find({isActive : true}).then(result => {
        if(result.length === 0){
            return res.send("There is currently no active courses");
        }else{
            return res.send(result);
        }
    })
}


module.exports.getCourse = (req, res) => {
    return Course.findById(req.params.courseID)
    .then(result =>{
        // if(result === 0){
        //     return res.send("Cannot find course with the ID provided.")
        // }else{
        //     return res.send(result);
        // }
        if(result === 0){
            return res.send("Cannot find course with the ID provided.")
        }else{
            if(result.isActive === false){
                return res.send ("the course you are trying to access is not available")
            }else{
                return res.send(result)
            }
        }
    })
    .catch(error => res.send("Please enter a correct course ID"));
}


module.exports.updateCourse = (req, res) =>{
    let updatedCourse = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
    return Course.findByIdAndUpdate(req.params.courseID, updatedCourse)
    .then((course, error) =>{
        if(error){
            return res.send(false);
        }else{
            return res.send(true);
        }
    })
}


module.exports.archiveCourse = (req,res) => {
    let archivedCourse = {
        isActive: req.body.isActive
    }
    return Course.findByIdAndUpdate(req.params.courseID, archivedCourse)
    .then((course,error) => {
        if(error){
            return res.send(false)
        }else{
            return res.send(true)
        }
    })
}


module.exports.activateCourse = (req,res) => {
    let activatedCourse = {
        isActive: true
    }
    return Course.findByIdAndUpdate(req.params.courseID, activatedCourse)
    .then((course,error) => {
        if(error){
            return res.send(false)
        }else{
            return res.send(true)
        }
    })
}