//[SECTION] Comparison Query Operator

// $gt / $gte operator (greater than/ greater than or equal to)

/* 
SYNTAX:

db.collectionName.find({field: {$gt: value}})
db.collectionName.find({field: {$gte: value}})

*/

db.users.find({age: {$gt: 50}});
db.users.find({age: {$gte: 50}});

// $lt / $lte operator (less than/ less than or equal to)

/* 
SYNTAX:

db.collectionName.find({field: {$gt: value}})
db.collectionName.find({field: {$gte: value}})

*/

db.users.find({age: {$lt: 50}});
db.users.find({age: {$lte: 50}});


// $ne  (not equal to)
/* 
SYNTAX:

db.collectionName.find({field: {$ne: value}})

*/
db.users.find({age: {$ne: 82}});


// $in  operator (for filtering)
/* 
SYNTAX:

db.collectionName.find({field: {$in: value}})

*/
db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});

//[SECTION] Logical Query Operator

/*  OR
SYNTAX:

db.collectionName.find({$or : [{fieldA: value}, {fieldB: value}]})

*/

db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

/*  AND (should have two field values)
SYNTAX:

db.collectionName.find({$or : [{fieldA: value}, {fieldB: value}]})

*/
db.users.find({$and: [
    {age: {$ne: 82}}, 
    {age: {$ne: 76}}
]
});


//[SECTION] Field Projection

/* 
SYNTAX:
db.collectionName.find({criteria}, {field: 1}]})

*/

//INCLUSION

db.users.find(
{
    firstName: "Jane"
},

{
    firstName: 1,
    lastName: 1,
    contact: 1
}
);

db.users.find(
    {
        firstName: "Jane"
    },
    
    {
        firstName: 1,
        lastName: 1,
        "contact.email": 1,
        
    }
    );
    
//EXCLUSION
db.users.find(
    {
        firstName: "Jane"
    },
    
    {
        contact: 0,
        department: 0
    }
    );

    //Suppressing the ID Field

    db.users.find(
        {
            firstName: "Jane"
        },
        
        {
            firstName: 1,
            lastName: 1,
            contact: 1,
            _id: 0
        }
        );

    
        //[SECTION] Evaluation Query Operator

        //$regex operator

        /* 
        SYNTAX: 
        db.collectionName.find({field: {$regex : "pattern", $options: "optionValue"}})
        */

        //Case Sensitive Query
        db.users.find({firstName: {$regex: "N"}});


        //Case Insensitive Query
        db.users.find({firstName: {$regex: "j", $options: "i"}});
        db.users.find({firstName: {$regex: "n", $options: "i"}});


        
    