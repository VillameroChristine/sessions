db.fruits.insertMany([
    {
      name : "Apple",
      color : "Red",
      stock : 20,
      price: 40,
      supplier_id : 1,
      onSale : true,
      origin: [ "Philippines", "US" ]
    },

    {
      name : "Banana",
      color : "Yellow",
      stock : 15,
      price: 20,
      supplier_id : 2,
      onSale : true,
      origin: [ "Philippines", "Ecuador" ]
    },

    {
      name : "Kiwi",
      color : "Green",
      stock : 25,
      price: 50,
      supplier_id : 1,
      onSale : true,
      origin: [ "US", "China" ]
    },

    {
      name : "Mango",
      color : "Yellow",
      stock : 10,
      price: 120,
      supplier_id : 2,
      onSale : false,
      origin: [ "Philippines", "India" ]
    }
  ]);


  // Aggregate Methods

  /* 
  SYNTAX:

  - {$match: {field: value}}
  -$group - group elements together

  db.collectionName.aggregate([
    {match: {fieldA :valuaA}},
    {$group: {_id: "$fieldB"}, {result: {operation}}}
  ])

  $sum - to get the total value

   */

  db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
  ]);

  //Field Projection with Aggregation

  //$project -  can be used when aggregating data to include/exclude fields from the returned results.
//Project values: 1 and 0 (to show field or not)
  db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    {$project: {_id: 0}}
  ]);

  //Sorting aggregated result
// Sorting: -1 desccending, 1 ascending
  db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    {$project: {_id: 0}},
    {$sort: {total: 1}}
  ]);

  //Aggregating result based on array fields

  //{$unwind: field}
  //creates individual copies for ARRAY FIELDS only, usually contains only one field

  db.fruits.aggregate([
    {$unwind: "$origin"}
  ]);

//Display fruit documents by their origin and the kinds of fruits that are supplied
// $sum: 1 (to enable the command $sum , put 1 as value)

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin", kinds: {$sum: 1}}}
  ]);