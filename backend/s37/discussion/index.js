// Node.js
/* 
a client-server architecture

Client applications --> browsers
DB servers --> systems developed using node
DB --> MongoDB

Benefits
1. Performance -> optimized for web applications
2. Familiarity - code is same old JS

NPM - Node Package Manager
*/

//Use the "require" directive to load Node.js
//"http module" contains the components needed to create a server
let http = require("http");

/* creating server, calling server 
function has always 2 parameter:
request - from the user
response - from the server

"createServer()" listens to a request from the client.
A port is a virtual point where network connection starts and end.
".listen()" is where the server will listen to any request sent in our server.

*/
http.createServer(function(request, response){

    //"writeHead()" sets the status code for response
    //status code 200 means OK
    response.writeHead(200,{"Content-Type": "text/plain"});

    //sends the response with the text content "Hello world!"
    response.end("Hello World!");

}).listen(4000); 

//output will be shown in the terminal
console.log("serrver is running at localhost:4000")

