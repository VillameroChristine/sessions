const express = require("express");
const router = express.Router();
//allows access to HTTP Methods and middlewares

const taskController = require("../controllers/taskController.js")

router.get("/", (req,res)=> {
    taskController.getAllTask().then(resultFromController => res.send(resultFromController))
    console.log("Hello world")
});


//create task

router.post("/", (req,res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//delete a task using wildcard on params
router.delete("/:id", (req,res)=> {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


//updatte a task

router.put("/:id", (req,res)=> {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


//=====================ACTIVITY==============================

router.get("/:id", (req,res)=> {
    taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
});


router.put("/:id/:status", (req,res)=> {
    taskController.updateTaskStatus(req.params).then(resultFromController => res.send(resultFromController))
})

module.exports = router;